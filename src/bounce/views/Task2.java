package bounce.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import bounce.NestingShape;
import bounce.Shape;
import bounce.ShapeModel;
import bounce.ShapeModelEvent;
import bounce.ShapeModelListener;

public class Task2 implements TreeModel, ShapeModelListener {
	
	private ShapeModel _sm;
	private List<TreeModelListener> _listeners;
	
	public Task2(ShapeModel sm) {
		_sm = sm;
		_listeners = new ArrayList<TreeModelListener>();
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		_listeners.add(l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		_listeners.remove(l);

	}
	@Override
	public Object getChild(Object parent, int index) {
		if(!(parent instanceof NestingShape))
			return null;
		else {
			if(this.getChildCount(parent) > index && index >= 0) {
				return ((NestingShape)parent).shapeAt(index);
			}
			else 
				return null;
		}
	}

	@Override
	public int getChildCount(Object parent) {
		if(!(parent instanceof NestingShape)) {
			return 0;
		}
		else 
			return ((NestingShape)parent).shapeCount();
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {

		if(!(parent instanceof NestingShape)) {
			return -1;
		}
		else {
			if(child instanceof Shape)
				return ((NestingShape)parent).indexOf((Shape)child);
			else
				return -1;
		}
	}

	@Override
	public Object getRoot() {
		return _sm.root();
	}

	@Override
	public boolean isLeaf(Object node) {
		if(node instanceof Shape) {
			if(!(node instanceof NestingShape))
				return true;
			else {
				return false;
				//return (this.getChildCount(node) == 0 ? true : false);
			}
		}
		return false;
	}


	@Override
	public void valueForPathChanged(TreePath arg0, Object arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(ShapeModelEvent event) {
		int[] index = new int[]{event.index()};
		ShapeModel source = event.source();
		Shape[] child = new Shape[]{event.operand()};
		List<Shape> pathList = new ArrayList<>();
		Shape parent = event.parent();

		while(parent != null) {
			pathList.add(parent);
			parent = parent.parent();
		}

		Collections.reverse(pathList);
		Object[] arr = new Object[pathList.size()];
		arr = pathList.toArray(arr);
		/*
		List<Shape>  list = event.operand().path();
		list.remove(event.operand());
		arr = list.toArray(arr);
		 */
		switch(event.eventType()) {
		case ShapeAdded:
			for(TreeModelListener ls: _listeners) {
				ls.treeNodesInserted(new TreeModelEvent(source, arr, index, child));
			}
			break;
		case ShapeMoved:
			//for(TreeModelListener ls: _listeners) {
			//	ls.treeNodesChanged(new TreeModelEvent(source, arr, index, child));
			//}
			break;
		case ShapeRemoved:
			for(TreeModelListener ls: _listeners) {
				ls.treeNodesRemoved(new TreeModelEvent(source, arr, index, child));
			}
			break;
		default:
			break;

		}

	}

}

