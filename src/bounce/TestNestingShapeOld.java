package bounce;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.awt.Color;
import java.security.AccessControlContext;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.css.Rect;

import junit.framework.AssertionFailedError;

public class TestNestingShapeOld {
	
	private MockPainter _painter;
	
	@Before
	public void setUp() {
		_painter = new MockPainter();
	}
	
	@Test 
	public void testNestingShapeCreation() {
		NestingShape nestingShape = new NestingShape();
		nestingShape.paint(_painter);
		assertEquals("(rectangle 0,0,25,35)(translate 0,0)"
				+ "(translate 0,0)", _painter.toString());
	}
	
	@Test
	public void testNestingShapeSimpleMove() {
		NestingShape nestingShape = new NestingShape();
		nestingShape.paint(_painter);
		nestingShape.move(10000, 10000);
		nestingShape.paint(_painter);
		assertEquals("(rectangle 0,0,25,35)(translate 0,0)"
				+ "(translate 0,0)(rectangle 5,5,25,35)"
				+ "(translate 5,5)(translate -5,-5)",
				_painter.toString());
	}
	
	@Test 
	public void testNestingShapeThreeNestCreation() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		RectangleShape rectangleShape = new RectangleShape();
		root.add(inter);
		inter.add(rectangleShape);
		List<Shape> rectShapePath = rectangleShape.path();
		List<Shape> exp = new ArrayList<>();
		exp.add(root);
		exp.add(inter);
		exp.add(rectangleShape);
		assertEquals(exp, rectShapePath);
		assertEquals(null, root.parent());
		assertEquals(root, inter.parent());
		assertEquals(inter, rectangleShape.parent());
	}
	
	@Test
	public void testNestingShapePathFiveMultiNesting() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		NestingShape inter_2 = new NestingShape(20, 60, -8, 1, 100, 100);
		RectangleShape rectangleShape = new RectangleShape();
		OvalShape ovalShape = new OvalShape();
		root.add(inter);
		root.add(inter_2);
		root.add(ovalShape);
		inter.add(rectangleShape);
		List<Shape> rectShapePath = rectangleShape.path();
		List<Shape> exp = new ArrayList<>();
		
		exp.add(root);
		exp.add(inter);
		exp.add(rectangleShape);
		assertEquals(exp, rectShapePath);
		
		List<Shape> ovalShapePath = ovalShape.path();
		exp = new ArrayList<>();
		exp.add(root);
		exp.add(ovalShape);
		assertEquals(exp, ovalShapePath);
		
		
		assertEquals(null, root.parent());
		assertEquals(root, inter.parent());
		assertEquals(inter, rectangleShape.parent());
		assertEquals(root, inter_2.parent());
		assertEquals(root, ovalShape.parent());
	}
	
	@Test
	public void testNestingShapeDeletion() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		RectangleShape rectangleShape = new RectangleShape();
		root.add(inter);
		inter.add(rectangleShape);

		inter.remove(rectangleShape);
		assertNull(rectangleShape.parent());
		
		root.remove(inter);
		assertNull(inter.parent());
	}
	
	@Test
	public void testNestingShapeGetShapeForIndex() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		RectangleShape rectangleShape = new RectangleShape();
		root.add(inter);
		inter.add(rectangleShape);
		
		Shape foundShape = root.shapeAt(0);
		assertEquals(inter, foundShape);

		foundShape = inter.shapeAt(0);
		assertEquals(rectangleShape, foundShape);
	}
	
	@Test
	public void testNestingShapeCountShapes() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		NestingShape inter_2 = new NestingShape(20, 60, -8, 1, 100, 100);
		RectangleShape rectangleShape = new RectangleShape();
		OvalShape ovalShape = new OvalShape();
		
		root.add(inter);
		root.add(inter_2);
		root.add(ovalShape);
		inter.add(rectangleShape);
		
		int countRootLevel = root.shapeCount();
		assertEquals(3, countRootLevel);
		int countInterLevel = inter.shapeCount();
		assertEquals(1, countInterLevel);
		int countBottomLevel = inter_2.shapeCount();
		assertEquals(0, countBottomLevel);
		
	}
	
	@Test
	public void testNestingShapeFindIndexOfShape() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		NestingShape inter_2 = new NestingShape(20, 60, -8, 1, 100, 100);
		RectangleShape rectangleShape = new RectangleShape();
		OvalShape ovalShape = new OvalShape();
		
		root.add(inter);
		root.add(inter_2);
		root.add(ovalShape);
		inter.add(rectangleShape);
		
		int index;
		index = root.indexOf(inter);
		assertEquals(0, index);
		index = root.indexOf(inter_2);
		assertEquals(1, index);
		index = root.indexOf(ovalShape);
		assertEquals(2, index);
		index = root.indexOf(rectangleShape);
		assertEquals(-1, index);
	}
	
	@Test
	public void testNestingShapeContains() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		NestingShape inter_2 = new NestingShape(20, 60, -8, 1, 100, 100);
		RectangleShape rectangleShape = new RectangleShape();
		OvalShape ovalShape = new OvalShape();
		
		root.add(inter);
		root.add(inter_2);
		root.add(ovalShape);
		inter.add(rectangleShape);	
		
		boolean contains;
		contains = inter.contains(rectangleShape);
		assertTrue(contains);
		contains = inter.contains(ovalShape);
		assertFalse(contains);
		contains = root.contains(inter_2);
		assertTrue(contains);
		contains = root.contains(rectangleShape);
		assertFalse(contains);
		}
	
	@Test
	public void testNestingShapeAlreadyParentAdd() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape alt_root = new NestingShape(40, 80, 8, 15, 500, 600);
		RectangleShape rectangleShape = new RectangleShape();
		root.add(rectangleShape);
		try {
			alt_root.add(rectangleShape);
			throw new AssertionFailedError();
		}
		catch(Exception e) {
			if(!(e instanceof IllegalArgumentException)) {
				throw new AssertionFailedError();
			}
		}
		
	}
	
	@Test
	public void testNestingShapeLargerInnerShape() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inner = new NestingShape(40, 80, 8, 15, 500, 600);
		try {
			root.add(inner);
			throw new AssertionFailedError();
		}
		catch(Exception e) {
			if(!(e instanceof IllegalArgumentException)) {
				throw new AssertionFailedError();
			}
		}
		
	}
	
	@Test
	public void testNestingShapeFiveNestMove() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		NestingShape inter_2 = new NestingShape(20, 60, -8, 1, 100, 100);
		RectangleShape rectangleShape = new RectangleShape();
		OvalShape ovalShape = new OvalShape();
		
		root.add(inter);
		root.add(inter_2);
		root.add(ovalShape);
		inter.add(rectangleShape);	
		
		root.paint(_painter);
		root.move(10000, 10000);
		root.paint(_painter);
		
		assertEquals("(rectangle 40,80,400,500)(translate 40,80)"
				+ "(rectangle 40,80,100,100)(translate 40,80)"
				+ "(rectangle 0,0,25,35)(translate -40,-80)"
				+ "(rectangle 20,60,100,100)(translate 20,60)"
				+ "(translate -20,-60)(oval 0,0,25,35)"
				+ "(translate -40,-80)(rectangle 58,77,400,500)"
				+ "(translate 58,77)(rectangle 48,95,100,100)"
				+ "(translate 48,95)(rectangle 5,5,25,35)"
				+ "(translate -48,-95)(rectangle 12,61,100,100)"
				+ "(translate 12,61)(translate -12,-61)(oval 5,5,25,35)"
				+ "(translate -58,-77)", _painter.toString());
	}
	
	@Test
	public void testNestingShapeFiveNestBounce() {
		NestingShape root = new NestingShape(40, 10, 18, -15, 400, 500);
		NestingShape inter = new NestingShape(5, 390, -8, 15, 100, 100);
		NestingShape inter_2 = new NestingShape(20, 60, -8, 1, 100, 100);
		OvalShape ovalShape = new OvalShape(10, 90, -20, 19, 20, 20);
		RectangleShape rectangleShape = new RectangleShape(90, 90, 20, 20, 10, 10);
		
		root.add(inter);
		root.add(inter_2);
		root.add(ovalShape);
		inter.add(rectangleShape);	
		
		root.paint(_painter);
		root.move(450, 860);
		root.paint(_painter);
		
		assertEquals("(rectangle 40,10,400,500)(translate 40,10)"
				+ "(rectangle 5,390,100,100)(translate 5,390)"
				+ "(rectangle 90,90,10,10)(translate -5,-390)"
				+ "(rectangle 20,60,100,100)(translate 20,60)"
				+ "(translate -20,-60)(oval 10,90,20,20)"
				+ "(translate -40,-10)(rectangle 50,0,400,500)"
				+ "(translate 50,0)(rectangle 0,400,100,100)"
				+ "(translate 0,400)(rectangle 90,90,10,10)"
				+ "(translate 0,-400)(rectangle 12,61,100,100)"
				+ "(translate 12,61)(translate -12,-61)"
				+ "(oval 0,109,20,20)(translate -50,0)", _painter.toString());
	}
	
	
	@Test
	public void testNestingShapeThreeNestMove() {
		NestingShape root = new NestingShape(40, 80, 18, -3, 400, 500);
		NestingShape inter = new NestingShape(40, 80, 8, 15, 100, 100);
		root.add(inter);
		inter.add(new OvalShape());
		root.paint(_painter);
		root.move(10000, 10000);
		root.paint(_painter);
		assertEquals("(rectangle 40,80,400,500)(translate 40,80)"
				+ "(rectangle 40,80,100,100)(translate 40,80)"
				+ "(oval 0,0,25,35)(translate -40,-80)(translate -40,-80)"
				+ "(rectangle 58,77,400,500)(translate 58,77)"
				+ "(rectangle 48,95,100,100)(translate 48,95)"
				+ "(oval 5,5,25,35)(translate -48,-95)"
				+ "(translate -58,-77)", _painter.toString());
	}
	
	@Test
	public void testNestingShapeThreeNestBounce() {
		NestingShape root = new NestingShape(40, 10, 18, -15, 400, 500);
		NestingShape inter = new NestingShape(5, 390, -8, 15, 100, 100);
		OvalShape ovalShape = new OvalShape(10, 90, -20, 19, 20, 20);
		root.add(inter);
		inter.add(ovalShape);
		root.paint(_painter);
		root.move(450, 10000);
		root.paint(_painter);
		assertEquals("(rectangle 40,10,400,500)(translate 40,10)"
				+ "(rectangle 5,390,100,100)(translate 5,390)"
				+ "(oval 10,90,20,20)(translate -5,-390)(translate -40,-10)"
				+ "(rectangle 50,0,400,500)(translate 50,0)"
				+ "(rectangle 0,400,100,100)(translate 0,400)"
				+ "(oval 0,80,20,20)(translate 0,-400)(translate -50,0)", _painter.toString());
	}
	
	
}