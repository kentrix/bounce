package bounce;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.print.Printable;

/**
 * Implementation of the Painter interface that delegates drawing to a
 * java.awt.Graphics object.
 * 
 * @author Ian Warren
 * 
 */
public class GraphicsPainter implements Painter {
	// Delegate object.
	private Graphics _g;

	/**
	 * Creates a GraphicsPainter object and sets its Graphics delegate.
	 */
	public GraphicsPainter(Graphics g) {
		this._g = g;
	}

	/**
	 * @see bounce.Painter.drawRect
	 */
	public void drawRect(int x, int y, int width, int height) {
		_g.drawRect(x, y, width, height);
	}

	/**
	 * @see bounce.Painter.drawOval
	 */
	public void drawOval(int x, int y, int width, int height) {
		_g.drawOval(x, y, width, height);
	}

	/**
	 * @see bounce.Painter.drawLine.
	 */
	public void drawLine(int x1, int y1, int x2, int y2) {
		_g.drawLine(x1, y1, x2, y2);
	}

	@Override
	public void fillRect(int x, int y, int width, int height) {
		_g.fillRect(x, y, width, height);
	}

	@Override
	public Color getColor() {
		return _g.getColor();
	}

	@Override
	public void setColor(Color c) {
		_g.setColor(c);
	}

	@Override
	public void drawImage(Image img, int x, int y, int width, int height) {
		_g.drawImage(img, x, y, width, height, null);
	}

	@Override
	public void translate(int x, int y) {
		_g.translate(x, y);
	}

	@Override
	public void drawCentredText(String text, int x, int y, int width, int height) {

		FontMetrics fontMetrics = _g.getFontMetrics();
		int fontAscent = fontMetrics.getAscent();
		int fontDecent = fontMetrics.getDescent();
		int accentDiff = fontAscent > fontDecent ? ((fontAscent - fontDecent) / 2) : -((fontAscent - fontDecent) / 2);
		y += height/2 + accentDiff;
		x += width/2 - fontMetrics.stringWidth(text)/2;
		_g.drawString(text, x, y);
	}

	@Override
	public void setFont(Font font) {
		_g.setFont(font);
		
	}
}
