/**
 * 
 */
package bounce;

import java.util.ArrayList;


/**
 * @author edward
 *
 */
public class NestingShape extends Shape {

	private ArrayList<Shape> shapes = new ArrayList<>();

	public NestingShape(Shape... args){
		for(Object s : args) {
			if(s instanceof Shape) {
				shapes.add((Shape)s);
			}
		}
	}

	/**
	 * Creates a NestingShape object with default values for state
	 */
	public NestingShape(){
		super();
	}

	/**
	 *CreatesaNestingShapeobjectwithspecifiedlocationvalues,
	 *defaultvaluesforotherstateitems.
	 */
	public NestingShape(int x, int y){
		super(x, y);
	}

	/**
	 **CreatesaNestingShapewithspecifiedvaluesforlocation,velocity
	 **anddirection.Non−specifiedstateitemstakeondefaultvalues.
	 **/
	public NestingShape(int x, int y, int deltaX, int deltaY){
		super(x, y, deltaX, deltaY);
	}

	/**
	 **CreatesaNestingShapewithspecifiedvaluesforlocation,velocity,
	 **direction,widthandheight.
	 **/
	public NestingShape(int x, int y, int deltaX, int deltaY,
			int width, int height){
		super(x, y, deltaX, deltaY, width, height);
	}
	public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height, String text) {
		super(x,y,deltaX,deltaY,width,height,text);
	}

	/**
	 **Moves a NestingShape object (including its children) within the bounds
	 **specified by arguments width and height.
	 **/
	@Override
	public void move(int width,int height){
		super.move(width, height);
		for(Shape s : shapes) {
			s.move(_width, _height);
		}
	}

	void add(Shape shape) throws IllegalArgumentException {
		if(shape._width + shape._x > _width + _x || shape._height + shape._y > _height + _y) {
			throw new IllegalArgumentException();
		}
		if(shape.parent() != null) {
			throw new IllegalArgumentException();
		}
		if (shape == this) {
			throw new IllegalArgumentException();
		}
		shape._parent = this;
		shapes.add(shape);
		
	}
	
	void remove(Shape shape) {
		if(shapes.contains(shape)) {
			shapes.remove(shape);
			shape._parent = null;
		}
	}

	public Shape shapeAt(int index) throws IndexOutOfBoundsException {
		return shapes.get(index);
	}

	public int shapeCount() {
		return shapes.size();
	}

	public int indexOf(Shape shape) {
		return shapes.indexOf(shape);
	}
	
	public boolean contains(Shape shape) {
		return shapes.contains(shape);
	}
	/* (non-Javadoc)
	 * @see bounce.Shape#paint(bounce.Painter)
	 */
	@Override
	protected void doPaint(Painter painter) {
		//paints itself first
		painter.drawRect(_x, _y, _width, _height);
		painter.translate(_x, _y);
		for(Shape s : shapes) {
			s.paint(painter);
		}
		painter.translate(-_x , -_y);
	}

}
