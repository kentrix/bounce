package bounce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestOvalShape {
	
	private MockPainter _painter;
	
	@Before
	public void setUp() {
		_painter = new MockPainter();
	}
	
	@Test
	public void testOvalSimpleMove() {
		OvalShape ovalShape = new OvalShape(70, 100, 15, 20);
		ovalShape.paint(_painter);
		ovalShape.move(10000, 10000);
		ovalShape.paint(_painter);
		assertEquals("(oval 70,100,25,35)(oval 85,120,25,35)", 
				_painter.toString());
	}
	
		/**
	 * Test to perform a bounce movement off the right-most boundary and to
	 * ensure that the Shape's position after the movement is correct.
	 */
	@Test
	public void testOvalMoveWithBounceOffRight() {
		OvalShape ovalShape = new OvalShape(105, 100, 15, 20);
		ovalShape.paint(_painter);
		ovalShape.move(135, 10000);
		ovalShape.paint(_painter);
		ovalShape.move(135, 10000);
		ovalShape.paint(_painter);
		assertEquals("(oval 105,100,25,35)(oval 110,120,25,35)"
				+ "(oval 95,140,25,35)", _painter.toString());
	}

	/**
	 * Test to perform a bounce movement off the left-most boundary and to
	 * ensure that the Shape's position after the movement is correct.
	 */
	@Test
	public void testShapeMoveWithBounceOffLeft() {
		OvalShape ovalShape = new OvalShape(7, 100, -15, 20);
		ovalShape.paint(_painter);
		ovalShape.move(10000, 10000);
		ovalShape.paint(_painter);
		ovalShape.move(10000, 10000);
		ovalShape.paint(_painter);
		assertEquals("(oval 7,100,25,35)(oval 0,120,25,35)"
				+ "(oval 15,140,25,35)", _painter.toString());
	}

	/**
	 * Test to perform a bounce movement off the bottom right corner and to
	 * ensure that the Shape's position after the movement is correct.
	 */
	@Test
	public void testShapeMoveWithBounceOffBottomAndRight() {
		OvalShape ovalShape = new OvalShape(10, 90, -13, 20);
		ovalShape.paint(_painter);
		ovalShape.move(125, 135);
		ovalShape.paint(_painter);
		ovalShape.move(125, 135);
		ovalShape.paint(_painter);
		assertEquals("(oval 10,90,25,35)(oval 0,100,25,35)"
				+ "(oval 13,80,25,35)", _painter.toString());
	}

}
