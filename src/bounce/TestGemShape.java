package bounce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestGemShape {
	
	private MockPainter _painter;
	
	@Before
	public void setUp() {
		_painter = new MockPainter();
	}
	
	@Test
	public void testNormalCreation() {
		GemShape gemShape = new GemShape(10, 20, 30, 10, 60, 30);
		gemShape.paint(_painter);
		assertEquals("(line 10,35,30,20)(line 30,20,50,20)" +
					"(line 50,20,70,35)(line 70,35,50,50)" +
					"(line 50,50,30,50)(line 30,50,10,35)",
				_painter.toString());
	}
	
	@Test
	public void testSmallCreation() {
		GemShape gemShape = new GemShape(10, 20, 30, 10, 30, 30);
		gemShape.paint(_painter);
		assertEquals("(line 10,35,25,20)(line 25,20,25,20)" +
					"(line 25,20,40,35)(line 40,35,25,50)" +
					"(line 25,50,25,50)(line 25,50,10,35)",
				_painter.toString());
	}
	
	@Test
	public void testGemSimpleMove() {
		GemShape gemShape = new GemShape(70, 100, 15, 20);
		gemShape.paint(_painter);
		gemShape.move(10000, 10000);
		gemShape.paint(_painter);
		assertEquals("(line 70,117,82,100)(line 82,100,82,100)"
				+ "(line 82,100,95,117)(line 95,117,82,135)"
				+ "(line 82,135,82,135)(line 82,135,70,117)"
				+ "(line 85,137,97,120)(line 97,120,97,120)"
				+ "(line 97,120,110,137)(line 110,137,97,155)"
				+ "(line 97,155,97,155)(line 97,155,85,137)", 
				_painter.toString());
	}
	
		/**
	 * Test to perform a bounce movement off the right-most boundary and to
	 * ensure that the Shape's position after the movement is correct.
	 */
	@Test
	public void testGemMoveWithBounceOffRight() {
		GemShape gemShape = new GemShape(105, 100, 15, 20);
		gemShape.paint(_painter);
		gemShape.move(135, 10000);
		gemShape.paint(_painter);
		gemShape.move(135, 10000);
		gemShape.paint(_painter);
		assertEquals("(line 105,117,117,100)(line 117,100,117,100)"
				+ "(line 117,100,130,117)(line 130,117,117,135)"
				+ "(line 117,135,117,135)(line 117,135,105,117)"
				+ "(line 110,137,122,120)(line 122,120,122,120)"
				+ "(line 122,120,135,137)(line 135,137,122,155)"
				+ "(line 122,155,122,155)(line 122,155,110,137)"
				+ "(line 95,157,107,140)(line 107,140,107,140)"
				+ "(line 107,140,120,157)(line 120,157,107,175)"
				+ "(line 107,175,107,175)(line 107,175,95,157)", _painter.toString());
	}

	/**
	 * Test to perform a bounce movement off the left-most boundary and to
	 * ensure that the Shape's position after the movement is correct.
	 */
	@Test
	public void testShapeMoveWithBounceOffLeft() {
		GemShape gemShape = new GemShape(7, 100, -15, 20);
		gemShape.paint(_painter);
		gemShape.move(10000, 10000);
		gemShape.paint(_painter);
		gemShape.move(10000, 10000);
		gemShape.paint(_painter);
		assertEquals("(line 7,117,19,100)(line 19,100,19,100)"
				+ "(line 19,100,32,117)(line 32,117,19,135)"
				+ "(line 19,135,19,135)(line 19,135,7,117)"
				+ "(line 0,137,12,120)(line 12,120,12,120)"
				+ "(line 12,120,25,137)(line 25,137,12,155)"
				+ "(line 12,155,12,155)(line 12,155,0,137)"
				+ "(line 15,157,27,140)(line 27,140,27,140)"
				+ "(line 27,140,40,157)(line 40,157,27,175)"
				+ "(line 27,175,27,175)(line 27,175,15,157)", _painter.toString());
	}

	/**
	 * Test to perform a bounce movement off the bottom right corner and to
	 * ensure that the Shape's position after the movement is correct.
	 */
	@Test
	public void testShapeMoveWithBounceOffBottomAndRight() {
		GemShape gemShape = new GemShape(10, 90, -13, 20);
		gemShape.paint(_painter);
		gemShape.move(125, 135);
		gemShape.paint(_painter);
		gemShape.move(125, 135);
		gemShape.paint(_painter);
		assertEquals("(line 10,107,22,90)(line 22,90,22,90)"
				+ "(line 22,90,35,107)(line 35,107,22,125)"
				+ "(line 22,125,22,125)(line 22,125,10,107)"
				+ "(line 0,117,12,100)(line 12,100,12,100)"
				+ "(line 12,100,25,117)(line 25,117,12,135)"
				+ "(line 12,135,12,135)(line 12,135,0,117)"
				+ "(line 13,97,25,80)(line 25,80,25,80)"
				+ "(line 25,80,38,97)(line 38,97,25,115)"
				+ "(line 25,115,25,115)(line 25,115,13,97)", _painter.toString());
	}

}

