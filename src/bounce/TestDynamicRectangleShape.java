package bounce;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

public class TestDynamicRectangleShape {
	
	private MockPainter _painter;
	
	@Before
	public void setUp() {
		_painter = new MockPainter();
	}
	
	@Test
	public void testDynamicRectangleSimpleMove() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(70, 100, 15, 20, Color.RED);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(10000, 10000);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 70,100,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 85,120,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", 
				_painter.toString());
	}
	
		/**
	 * Test to perform a bounce movement off the right-most boundary and to
	 * ensure that the Shape's position after the movement is correct.
	 */
	@Test
	public void testDynamicRectangleMoveWithBounceOffRight() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(105, 100, 15, 20, Color.GREEN);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(135, 10000);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(135, 10000);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 105,100,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 110,120,25,35)"
				+ "(setColor java.awt.Color[r=0,g=255,b=0]"
				+ "(fillRect 110,120,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 95,140,25,35)"
				+ "(setColor java.awt.Color[r=0,g=255,b=0]"
				+ "(fillRect 95,140,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", _painter.toString());
	}
	@Test
	public void testDynamicRectangleMoveWithBounceOffLeft() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(5, 100, -15, 20, Color.RED);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(10000, 10000);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(10000, 10000);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 5,100,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 0,120,25,35)"
				+ "(setColor java.awt.Color[r=255,g=0,b=0]"
				+ "(fillRect 0,120,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 15,140,25,35)"
				+ "(setColor java.awt.Color[r=255,g=0,b=0]"
				+ "(fillRect 15,140,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", _painter.toString());
	}	@Test
	public void testDynamicRectangleMoveWithBounceOffTop() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(105, 10, 15, -20, Color.GREEN);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(10000, 10000);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(1000, 10000);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 105,10,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 120,0,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 135,20,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", _painter.toString());
	}	@Test
	public void testDynamicRectangleMoveWithBounceOffBottom() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(105, 65, 15, 20, Color.BLUE);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(10000, 100);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(10000, 100);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 105,65,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 120,65,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]("
				+ "rectangle 135,45,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", _painter.toString());
	}
	/**
	 * Test to perform a bounce movement off the left-most boundary and to
	 * ensure that the Shape's position after the movement is correct.
	 */


	/**
	 * Test to perform a bounce movement off the bottom right corner and to
	 * ensure that the Shape's position after the movement is correct.
	 */
	@Test
	public void testShapeMoveWithBounceOffBottomAndRight() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(105, 90, 15, 20, Color.RED);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(135, 135);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(135, 135);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 105,90,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 110,100,25,35)"
				+ "(setColor java.awt.Color[r=255,g=0,b=0]"
				+ "(fillRect 110,100,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 95,80,25,35)"
				+ "(setColor java.awt.Color[r=255,g=0,b=0]"
				+ "(fillRect 95,80,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", _painter.toString());
	}	
	
	@Test
	public void testShapeMoveWithBounceOffBottomAndLeft() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(10, 90, -13, 20, Color.GREEN);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(10000, 135);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(10000, 135);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 10,90,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 0,100,25,35)"
				+ "(setColor java.awt.Color[r=0,g=255,b=0]"
				+ "(fillRect 0,100,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 13,80,25,35)"
				+ "(setColor java.awt.Color[r=0,g=255,b=0]"
				+ "(fillRect 13,80,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", _painter.toString());
	}	
	@Test
	public void testShapeMoveWithBounceOffTopAndRight() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(105, 20, 15, -25, Color.BLUE);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(135, 10000);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(135, 10000);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 105,20,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 110,0,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=255]"
				+ "(fillRect 110,0,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 95,25,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=255]"
				+ "(fillRect 95,25,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", _painter.toString());
	}	
	@Test
	public void testShapeMoveWithBounceOffTopAndLeft() {
		DynamicRectangleShape dynamicRectShape = new DynamicRectangleShape(10, 10, -13, -20, Color.RED);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(125, 135);
		dynamicRectShape.paint(_painter);
		dynamicRectShape.move(125, 135);
		dynamicRectShape.paint(_painter);
		assertEquals("(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 10,10,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 0,0,25,35)"
				+ "(setColor java.awt.Color[r=255,g=0,b=0]"
				+ "(fillRect 0,0,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]"
				+ "(rectangle 13,20,25,35)"
				+ "(setColor java.awt.Color[r=255,g=0,b=0]"
				+ "(fillRect 13,20,25,35)"
				+ "(setColor java.awt.Color[r=0,g=0,b=0]", _painter.toString());
	}

}
