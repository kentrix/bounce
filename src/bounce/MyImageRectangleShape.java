package bounce;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class MyImageRectangleShape extends RectangleShape {
	
	private BufferedImage _img;
	/**
	 * Default constructor that creates a RectangleShape instance whose instance
	 * variables are set to default values.
	 */
	public MyImageRectangleShape() {
		super();
		try {
			_img = ImageIO.read(new File("default.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public MyImageRectangleShape(String fileName) {
		super();
		try {
			_img = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Creates a RectangleShape instance with specified values for instance 
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed and direction for horizontal axis.
	 * @param deltaY speed and direction for vertical axis.
	 */
	public MyImageRectangleShape(int x, int y, int deltaX, int deltaY) {
		super(x,y,deltaX,deltaY);
	}
	
	public MyImageRectangleShape(int x, int y, int deltaX, int deltaY, String fileName) {
		super(x,y,deltaX,deltaY);
		try {
			_img = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Creates a RectangleShape instance with specified values for instance 
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed (pixels per move call) and direction for horizontal 
	 *        axis.
	 * @param deltaY speed (pixels per move call) and direction for vertical 
	 *        axis.
	 * @param width width in pixels.
	 * @param height height in pixels.
	 */
	public MyImageRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
		super(x,y,deltaX,deltaY,width,height);
	}


	public MyImageRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) {
		super(x,y,deltaX,deltaY,width,height);
		try {
			_img = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doPaint(Painter painter) {
		painter.drawRect(_x,_y,_width,_height);
		painter.drawImage(_img, _x, _y, _width, _height);
	}

}
