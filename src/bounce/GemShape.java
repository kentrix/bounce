package bounce;

import java.util.ArrayList;


public class GemShape extends Shape {

	private static final int SHIFT = 20;
	/**
	 * Default constructor that creates a GemShape instance whose instance
	 * variables are set to default values.
	 */
	public GemShape() {
		super();
	}
	
	/**
	 * Creates a GemShape instance with specified values for instance 
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed and direction for horizontal axis.
	 * @param deltaY speed and direction for vertical axis.
	 */
	public GemShape(int x, int y, int deltaX, int deltaY) {
		super(x,y,deltaX,deltaY);
	}

	
	/**
	 * Creates a GemShape instance with specified values for instance 
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed (pixels per move call) and direction for horizontal 
	 *        axis.
	 * @param deltaY speed (pixels per move call) and direction for vertical 
	 *        axis.
	 * @param width width in pixels.
	 * @param height height in pixels.
	 */
	public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
		super(x,y,deltaX,deltaY,width,height);
	}
	
	public GemShape(int x, int y, int deltaX, int deltaY, int width, int height, String text) {
		super(x,y,deltaX,deltaY,width,height,text);
	}

	private ArrayList<int[]> generateVertrices() {
		ArrayList<int[]> list = new ArrayList<>();
		if(_width >= 40) {
			list.add(new int[]{_x, _y + _height/2});
			list.add(new int[]{_x + SHIFT, _y});
			list.add(new int[]{_x + _width - SHIFT, _y});
			list.add(new int[]{_x + _width, _y + _height/2});
			list.add(new int[]{_x + _width - SHIFT, _y + _height});
			list.add(new int[]{_x + SHIFT, _y + _height});
		}
		else {
			list.add(new int[]{_x, _y + _height/2});
			list.add(new int[]{_x + _width/2, _y});
			list.add(new int[]{_x + _width/2, _y});
			list.add(new int[]{_x + _width, _y + _height/2});
			list.add(new int[]{_x + _width/2, _y + _height});
			list.add(new int[]{_x + _width/2, _y + _height});
		}
		return list;
	}	
	
	/**
	 * Paints this GemShape object using the supplied Painter object.
	 */
	@Override
	protected void doPaint(Painter painter) {
		ArrayList<int[]> list = this.generateVertrices();
		int[] currentVertex = list.get(0);
		for(int[] arr : list) {
			if(arr == currentVertex) {
				continue;
			}
			painter.drawLine(currentVertex[0], currentVertex[1], arr[0], arr[1]);
			currentVertex = arr;
		}
			painter.drawLine(currentVertex[0], currentVertex[1], list.get(0)[0], list.get(0)[1]);
	}
}
