package bounce;

import java.awt.Color;

public class DynamicRectangleShape extends RectangleShape {
	
	private Color _color = Color.BLACK;
	private boolean _solid = false;
	/**
	 * Default constructor that creates a RectangleShape instance whose instance
	 * variables are set to default values.
	 */
	public DynamicRectangleShape() {
		super();
	}
	
	public DynamicRectangleShape(Color c) {
		super();
		_color = c;
	}	
	
	/**
	 * Creates a RectangleShape instance with specified values for instance 
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed and direction for horizontal axis.
	 * @param deltaY speed and direction for vertical axis.
	 */
	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
		super(x,y,deltaX,deltaY);
	}
	
	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, Color c) {
		super(x,y,deltaX,deltaY);
		_color = c;
	}
	
	/**
	 * Creates a RectangleShape instance with specified values for instance 
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed (pixels per move call) and direction for horizontal 
	 *        axis.
	 * @param deltaY speed (pixels per move call) and direction for vertical 
	 *        axis.
	 * @param width width in pixels.
	 * @param height height in pixels.
	 */
	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
		super(x,y,deltaX,deltaY,width,height);
	}

	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, String text, Color c) {
		super(x,y,deltaX,deltaY,width,height);
		_text = text;
		_color = c;
	}
	
	
	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color c) {
		super(x,y,deltaX,deltaY,width,height);
		_color = c;
	}
	
	@Override
	public void move(int width, int height) {
		super.move(width, height);
		
		if(_xBounce) {
			_xBounce = !_xBounce;
			_yBounce = false;
			_solid = true;
		}
		else if(_yBounce) {
			_yBounce = !_yBounce;
			_solid = false;
		}
	}
	

	
	/**
	 * Paints this RectangleShape object using the supplied Painter object.
	 */
	@Override
	protected void doPaint(Painter painter) {
		painter.setColor(Color.BLACK);
		painter.drawRect(_x,_y,_width,_height);
		if(_solid) {
			painter.setColor(_color);
			painter.fillRect(_x, _y, _width, _height);
		}
		painter.setColor(Color.BLACK);
	}
}
